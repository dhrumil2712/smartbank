import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

// Navigators
import { createDrawerNavigator, createStackNavigator, createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation'

// StackNavigator screens
import ItemList from './ItemList'
import Item from './Item'

// TabNavigator screens
import TabA from './TabA'
import TabB from './TabB'
import TabC from './TabC'
import review from './review'

// Plain old component
import Plain from './Plain'

export const Home = createStackNavigator({
  ItemList: { screen: ItemList },
  Item: { screen: Item },
}, {
  initialRouteName: 'ItemList',
})

export const AccountTransfer = createMaterialTopTabNavigator({
  From: { screen: TabA },
  To: { screen: TabB },
  Amount: { screen: TabC },
  Review: {screen: review}
}, {
  order: ['From', 'To', 'Amount', 'Review']
})

export const Drawer = createDrawerNavigator({
  Home: { screen: Home },
  AccountTransfer: { screen: AccountTransfer },
  Plain: { screen: Plain },
})
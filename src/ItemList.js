import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Button,
} from 'react-native'


const items = [
  { name: 'one'},
  { name: 'two'},
  { name: 'three'},
  { name: 'four'},
]

class ItemList extends Component {

  static navigationOptions = {
    title: 'SmartBank'
  }

  renderItem = (item, i) => {
    return (
      <TouchableOpacity
        key={i}
        style={styles.item}
        onPress={() => this.props.navigation.navigate('Item', { title: item.name })}
      >
        <Text style={styles.itemText}>{item.name}</Text>
      </TouchableOpacity>)
  }

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Welcome to Smart Bank!</Text>
        {items.map(this.renderItem)}
        <Button style={styles.Button}
            title="SIGN IN"
            color='white'
            onPress={() => {
              this.props.navigation.navigate("AccountTransfer", {});
            }}
        />
      </View>
      )
  }
}
export default ItemList
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2980b9',
    padding: 20,
  },
  text: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold',
  },
  item: {
    padding: 10,
  },
  itemText: {
    color: 'white',
    fontSize: 20,
  },
  Button: {
    borderRadius: 2,
  },
})